# Inference Testing

In order to use this notebook, copy it to the /src folder in the ECS-RBM directory.  
  
First load in the necessary files from the source code:


```python
from Connector import SQL_connector
from utils import load_connection_dict, transform_KRI_table
from Infer import infer_risk
from datetime import date

import configparser
import sys
import logging
import pandas as pd

param_set = "Clintek_010"
config = configparser.ConfigParser()
config.read("../config/config.ini")
server = config[param_set]["server"]

connector_dict = load_connection_dict(config, server)
connector = SQL_connector(connector_dict)

study = config[param_set]["study"]
time_step = config[param_set]["time_step"]
lag_periods = config[param_set].getint("lag_periods")
path_to_saved_models = config[param_set]["path_to_saved_models"]
path_to_saved_KRIs = config[param_set]["path_to_saved_KRIs"]
path_to_saved_risk_scores = config[param_set]["path_to_saved_risk_scores"]
```

The following 2 methods help clean the KRI data output, if the cleaned output form is preferable, it is very easy to patch these into the source code (please let me know).


```python
def extract_KRI(row, KRI):
    """
    Extracts KRI information from the column-rich KRI format.
    
    Args:
        row: a row of a pandas dataframe
        KRI: the name of the KRI to extract
        
    Returns:
        a pandas dataframe containing bundled dictionaries
    """
    temp = {}
    temp["Site"] = row["Site"]
    temp["Reference_Date"] = row["Reference_Date"]
    temp["Type"] = KRI
    temp["Value"] = row[KRI]
    return temp

def clean_KRI_table(KRI_dataset, reference_date, lag_periods=2):
    """
    Cleans a raw_KRI table by removing extraneous sites and aligning date information.
    
    Args:
        KRI_dataset: a raw KRI dataset
        reference_date: the date on which the inference was performed
        lag_periods: the number of past steps to save
        
    Returns:
        a pandas dataframe
    """
    clean = transform_KRI_table(KRI_dataset, params={"lag_periods":lag_periods})
    clean_today = clean[clean["Start_Time"] == reference_date ]
    clean_today = clean_today.reset_index(drop=True).rename({"Start_Time": "Reference_Date"},axis=1)

    dfs = []
    for column in clean_today.columns:
        if column in ["Reference_Date", "Site", "site_active_next_period"]:
            pass
        else:
            value_dicts = list(clean_today.apply(lambda x: extract_KRI(x, column), axis=1).values)
            dfs.append(pd.DataFrame(value_dicts))

    out = pd.concat(dfs).reset_index(drop=True)
    return out
```

Set the parameters for the inference loop:


```python
inference_start_date = "2018-06-01"
inference_end_date = "2018-12-31"
inference_frequency = "15d"

model = "2018-06-01_30d_10_5.pkl"
```

Run the inference loop.  The results are saved locally in the dictionaries: raw_KRI_scores, clean_KRI_scores, and risk scores.  The keys for accessing data are the day on which the inference is performed.


```python
raw_KRI_scores = {}
clean_KRI_Scores = {}
risk_scores = {}

for reference_date in pd.date_range(inference_start_date, inference_end_date, freq=inference_frequency):
    print(f"Scoring {reference_date}")
    scores, KRIs = infer_risk(
    connector,
    study,
    load_model=model,
    time_step=time_step,
    params={
        "today": reference_date,
        "explain": False,
        "lag_periods": lag_periods,
        "path_to_saved_models": path_to_saved_models,
        "path_to_saved_KRIs": path_to_saved_KRIs,
        "path_to_saved_risk_scores": path_to_saved_risk_scores,
            },
        )
    KRI_scores[reference_date] = KRIs.dataset
    clean_KRI_Scores[reference_date] = clean_KRI_table(KRIs, reference_date)
    risk_scores[reference_date] = scores
```
