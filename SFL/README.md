# eClinical Solutions RBM 

## Purpose

The purpose of this project is to assess site risk for clinical trials by extracting Key Risk Indicators (KRIs) from historical clinical data. Using those KRI scores develop models to assess the real time risk of active sites in a clinical trial.

## Techniques and Methods

- Statistical analysis
- Risk based modeling
- Regression
  - LightGBM
- Outlier Detection
  - Isolation Forest
- Model Validation

### Project Description

The project can be divided into two main parts:
- __Model training__: To train a risk prediction model to distinguish between High Risk and Low Risk sites

- __Risk inference__: Use a pre-trained model to score the risk level and explain how risk assessments are being made. 

# Index
1. [Dependencies](#dependencies)
2. [Windows Installation](#windows-installation)
3. [Docker Installation](#docker-installation)
4. [Project Contents](#project-contents)
5. [Inputs](#inputs)
6. [Model Training Usage](#model-training-usage)
7. [Model Inference Usage](#model-inference-usage)
8. [Working with Historical Data](#working-with-historical-data)
9. [Folder Structure](#folder-structure)
10. [Licensing](#licensing)

## Dependencies

Install [Python version 3.7.7 or later](https://www.python.org/downloads/) to use on Windows.

For Fargate implementation install [Docker version 19.03 or later](https://docs.docker.com/get-docker/). 

## Windows Installation
To use the codebase on a Windows machine, navigate to the ECS-RBM code directory and execute
```
pip install -r requirements.txt
```
this will install all of the Python packages needed to run the model training and inference.

Additionally, the directories for data (KRIs and risk scores) need to be configured.  For example, the default configuration for the Clintek 10 study houses the KRI scores in '\data\Clintek_010\KRIs' and the risk scores in 'data\Clintek_010\risk'.  These paths can be re-configured by editing the 'PATH-TO-SAVED-KRIS' and 'PATH-TO-SAVED-RISK-SCORES' attributes in the config file.

## Docker Installation
A Docker image is included for use on AWS standalone machines; this file assumes that the machine is running Linux.

To build a docker image containing Python 3.7 and the required packages for executing model training and inference
1. To build the docker image, navigate to the `docker` folder, then run 
```
sudo docker build -t <image_name> -f Dockerfile .
```
where `<image_name>` is the docker image name (e.g. `rbm`). In this step, the `docker/requirement.txt` file is used to install all necessary libraries.


2. To launch the docker container, run
```
sudo docker run  -it --name <container_name> -v <repo_dir>:<mount_dir> <image_name>
```
where
- `<container_name>` is the docker container name (e.g. `rbm`).
- `<repo_dir>` is full path containing this repository folder on the host machine (e.g. `/home/ubuntu/rbm`).
- `<mount_dir>` is the path where this repository folder will be mounted inside the container (e.g. `/rbm_dir`). The `-v` option enables the access of contents of this repository folder through `<mount_dir>` in the docker container.

## Project Contents

The code for computing KRIs and training models can be found in src/classes, the relevant files are:

* `Dataset.py`: a basic dataset object with a load-from-file functionality (currently loads from a csv file, however the pyodbc connector will be implemented in next iteration) and various getter methods. 
* `Time_Range.py`: a time window object which allows KRIs to be computed using study day or calendar date, contains a type-checking method and an iterator method.
* `Connector.py`: a data ingestion object built to draw data from SQL tables
* `Counts.py`: the body of the KRI pipeline, contains methods for counting AEs, LBs, QYs, missed IP doses, for computing QY response times, and for computing variance in patient IP dosage. Also contains the rate functions for computing the AEs, LBs, QYs, and missed doses per active patient
* `Utils.py`: a collection of helper functions for preprocessing and reshaping datasets, and tallying the number of active patients per site
* `Score.py`: contains the score_KRIs method, which executes the KRI computation for a given time range
* `Train.py`: contains the train_ORM method for computing KRIs and training an ORM
* `Infer.py`: contains the infer_risk method for labeling sites as High or Low Risk

Additionally, there are two runtime algorithms:
* `train_ORM.py`: trains an ORM model pulling model configuration info. from the config directory
* `infer_risk.py`: infers site risk pulling configuration info. from the config directory

## Inputs

In SDTM format, unless otherwise noted

* **Demographics (DM) Table:** contains patient demographic information, the site and country data are most relevant
*  **Exposure (EX) Table:** contains the Investigational Product (IP) dosage information, used to deduce the active population of sites and various IP-related KRIs
* **Adverse Event (AE) Table:** contains adverse event patient information, used to compute several KRIs
* **Lab (LB) Table:** contains the lab test information for the study, used to compute data risk scores and relevant KRIs
* **Vital Sign (VS) Table:** contains the vital sign test information for the study, used to compute data risk scores and relevant KRIs
* **Query (QY) Table:** contains the data query information for the trial, in the Rave ODM format, used to compute relevant KRIs

## Model Training Usage

This usage trains (or retrains) and evaluates an Operational Risk Model. 

All modeling related configuration variables can be adjusted in `config/config.ini`, this includes the size of the prediction time step, the number of time steps to train and validate on, and where to save models and data.

To run the training algorithm, navigate to the 'src' directory and execute
```
python train_ORM.py
```
the algorithm will load the 'DEFAULT' configuration set (which is reserved for modeling), print the aggregate performance of the model and the Zero Knowledge Model, and save the model as a .pkl file in the 'models' directory.

## Model Inference Usage

To load a pre-trained model and infer site risk, navigate to 'src', in the CLI run:
```
python infer_risk.py <study_name>
```
where <study_name> is the name of the configuration set to use for the desired study; for example, running
```
python infer_risk.py Clintek_010
```
in the CLI would load the configuration parameters from the 'Clintek_010' header in the config file, and compute risk scores for the Clintek 10 study.  

The KRIs used to assess the risk scores are saved in the directory specified in the config file, similarly with the risk scores.

## Working with Historical Data
Currently, the config file for the model has a value defined for the 'today' attribute; when the code is in production mode the 'today' attribute should be deleted from the config file and the model training and inference will automatically update the date. To use historical data in the pipeline, simply change the 'today' attribute to the desired date.


## Folder Structure

    ├── LICENSE
    ├── README.md               <- top-level README for project developers
    ├── config					
    │   └── config.ini     	    <- config for model training/inference
    ├── data                    <- directory housing KRI/risk data, arranged by study  
    |   └── Clintek_010         <- data directories to hold each study
    |       └── KRIs            <- example KRI score table included here
    |       └── risk            <- example risk score table included here
    ├── docker                  
    │   └── Dockerfile          <- dockerfile 	    
    │   └── requirement.txt     <- requirements for the dockerfile
    ├── models            	    <- pre-trained models
    ├── notebooks				<- Jupyter notebooks      		 
    ├── reports  				<- Meeting notes and slides    		 
    ├── src               	
    │   ├── train_ORM.py        <- model training script
    │   ├── infer_risk.py       <- risk inference script
    │   ├── Dataset.py          <- dataset class
    │   ├── Time_Range.py       <- time range class
    │   ├── Connector.py        <- SQL connector class
    │   ├── Counts.py           <- methods for counting events
    │   ├── Score.py            <- methods for scoring KRIs
	│   ├── Models.py           <- model classes (ORM and Data Risk)
	│   ├── Train.py            <- methods for training models
	│   ├── Infer.py            <- ORM inference method
	│   ├── utils.py            <- helper methods



## Licensing

To the extent possible under the law, and under our agreements, [SFL Scientific](http://sflscientific.com/) retains all copyright and related or neighboring rights to this work.